package
{
	import flash.display.Sprite;
	import flash.geom.Point;

	public class DataReader
	{
		private  const ELLIPSE_WIDTH:Number = 40;
		
		private var _canvas:Sprite;
		private var _overlayLayer:Sprite;
		
		public  var data:Vector.<Vector.<Number>> = new Vector.<Vector.<Number>>;
		
		private var spline:Spline;
		
		public var bReverse:Boolean = false;
		
		public var controlPoints:Vector.<Point> = new Vector.<Point>;
		public var overPoints:Vector.<Boolean> = new Vector.<Boolean>;
		
		private var _x_controlPts:Vector.<Number> = new Vector.<Number>;
		
		
		public function DataReader(canvas:Sprite, 
								   overlayLayer:Sprite,
								   rawData:String,
								   numControlPoints:int = 20):void
		{	
			_canvas = canvas;
			_overlayLayer = overlayLayer;
			
			spline = new Spline;
			
			var i:int = 0;
			var j:int = 0;
			var b:int = 0;
			var p:int = 0;
			var temp:String = new String;
			
			if(rawData.charAt(0) == "r")
			{
				bReverse = true;
			}
			
				while(b < rawData.length - 1)
				{
				
					while(rawData.charAt(b) != "\t")
					{
						b ++;
					}
					
					b++;
					
					
					if(rawData.charAt(b) == "x")
					{
						b += 3;
						temp = "";
						
						while(rawData.charAt(b)  != "\t")
						{
							temp += rawData.charAt(b);
							b++;
						
						}
							
						data[j] = new Vector.<Number>;
						data[j][0] = Number(temp);
							
					}
					
					b += 1;
					
					if(rawData.charAt(b) == "y")
					{
						
						b += 3;
						temp = "";
						
						while(rawData.charAt(b) != "\n")
						{
						
							temp += rawData.charAt(b);
							b++;
							
						}
						
						
						data[j][1] = Number(temp);
						b++;
						j ++;
					}
					
				}
				
				var gapX:Number;
		
				if(!bReverse)
				{
					gapX = data[data.length-1][0] - data[i][0];
					
				}else{
					
					gapX = data[data.length-1][1] - data[i][1];
				}
				
				var stepControlX:Number = gapX / (numControlPoints - 1);
				
				
				if(!bReverse)
				{
					_x_controlPts[0] = data[i][0];
				
				}else{
				
					_x_controlPts[0] = data[i][1];	
				}
	
				
				for(i = 1; i < numControlPoints; i++)
				{
					
					_x_controlPts[i] = _x_controlPts[i-1] + stepControlX;
					
				}
				
				
				for(i = 0; i < data.length; i++)
				{
					for(j = 0; j < data[i].length; j++)
					{
						drawCircle(data[i][0], data[i][1]);
					}
				}
				
				var arY:Vector.<Number> = new Vector.<Number>;
				
				
				if(!bReverse)
				{
					for(i = 0; i < data.length; i++)
					{
						arY.push(data[i][1]);
					}
					
					for(i = 0; i < data.length; i++)
					{
							
							var tempY:Number = Spline.returnApproximateY(data[0][0], data[data.length - 1][0], data.length, arY, data[i][0]);
	
							drawCircle(data[i][0], tempY, 0xff0000, 0x0);
						
					}
				}else{
					
					for(i = 0; i < data.length; i++)
					{
						arY.push(data[i][0]);
					}
					
					for(i = 0; i < data.length; i++)
					{
						
						tempY = Spline.returnApproximateY(data[0][1], data[data.length - 1][1], data.length, arY, data[i][1]);
						
						drawCircle(tempY, data[i][1], 0xff0000, 0x0);
						
					}
				}
				
				for(i = 0; i < numControlPoints; i++)
				{
					if(!bReverse)
					{
						tempY = Spline.returnApproximateY(data[0][0], data[data.length - 1][0], data.length, arY, _x_controlPts[i]);
						controlPoints[i] = new Point(_x_controlPts[i], tempY);
						
					}else{
					
						tempY = Spline.returnApproximateY(data[0][1], data[data.length - 1][1], data.length, arY, _x_controlPts[i]);
						controlPoints[i] = new Point(tempY, _x_controlPts[i]);
					}
					
					overPoints[i] = false;
							
				}

		}
		
		
		private function drawCircle(x:Number, y:Number, color:uint = 0x333333, colorPoint:uint = 0x00ff00):void
		{
			if(_canvas)
			{
				
				_canvas.graphics.beginFill(color, 1);
				_canvas.graphics.drawEllipse(x - ELLIPSE_WIDTH/2, y - ELLIPSE_WIDTH/2, ELLIPSE_WIDTH, ELLIPSE_WIDTH);
				_canvas.graphics.endFill();
				
				_overlayLayer.graphics.beginFill(colorPoint, 1);
				_overlayLayer.graphics.drawEllipse(x, y, 5, 5);
				_overlayLayer.graphics.endFill();
			}
		}
	}
}