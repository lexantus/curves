package
{
	import com.bit101.components.InputText;
	import com.bit101.components.PushButton;
	
	import flash.display.Bitmap;
	import flash.display.CapsStyle;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import caurina.transitions.Tweener;
	
	import fl.motion.MatrixTransformer;

	
	[SWF(width="1280", height="800", backgroundColor="#333333")]
	public class Curves extends Sprite
	{

		private var fileStreamRead:FileStream;
		
		private var tfCurrentSegment:TextField;
		
		private var tfDeltas:Vector.<TextField> = new Vector.<TextField>;
		
		private var averageDeltas:Vector.<Number> = new Vector.<Number>;
		
		private var currentDeltas:Vector.<Number> = new Vector.<Number>;
		
		private var numDelta:int = 0;
		
		private var isMouseDown:Boolean = false;
		
		private var overlayLayer:Sprite;
		
		private var drawnCoords:Vector.<Vector.<Number>>;
		
		private var spline:Spline;
		
		private var tempArYForSpline:Vector.<Number> = new Vector.<Number>;
		
		private var bPoint1:Boolean = false;
		private var bPoint2:Boolean = false;
		
		private var bg:Sprite;
		
		
		private var userDeltaStartDrawY:Number = 0;
		private var userDeltaStartDrawX:Number = 0;
		
		// файлы наших сегментов
		private var curvesFiles:Vector.<File> = new Vector.<File>;
		
		// массив определяющий пораядок сегментов
		private var fileIndecies:Vector.<int> = new Vector.<int>;
		
		private var rawStrings:Vector.<ByteArray> = new Vector.<ByteArray>;
		private var dataReaders:Vector.<DataReader> = new Vector.<DataReader>;
		
		private var bMouseWasReleased:Boolean = false;
		
		private var textInputFileBase:InputText;
		private var btnStart:PushButton;
		
		private var totalContralPoints:int = 0;
		
		private var bFirst:Boolean = true;
		
		private var bFail:Boolean = false;
		
		
		
		private var linePoints:Vector.<LinePoint> = new Vector.<LinePoint>;
		private var curveVerticies:Vector.<Object> = new Vector.<Object>;
		
		private var endPoint:LinePoint = new LinePoint;
		private var beginPoint:LinePoint = new LinePoint;
		
		private var bFirstSegment:Boolean = true;
		
		[Embed(source="pattern01.png")]
		private static var PATTERN1:Class;
		
		[Embed(source="pattern02.png")]
		private static var PATTERN2:Class;
		
		[Embed(source="pattern03.png")]
		private static var PATTERN3:Class;
		
		[Embed(source="pattern07.png")]
		private static var PATTERN7:Class;
		
		private const PATTERNS_SMALL:Vector.<Class> =  new <Class>[PATTERN1, 
																   PATTERN2,
																   PATTERN3];
		
		private const PATTERNS_BIG:Vector.<Class> =  new <Class>[PATTERN7];
		
		
		private var privX:Number = 0;
		private var privY:Number = 0;
		
		public function Curves()
		{
			
			textInputFileBase = new InputText(this, 1125, 600, "");
			btnStart = new PushButton(this, 1125, 640, "Start", function():void{
			
				//startGame();
			});
			
		
			
			var ptrn1:Bitmap = new PATTERN1;
			ptrn1.x = 100;
			ptrn1.y = 100;
			addChild(ptrn1);
			
			var pnt1:Shape = new Shape;
				pnt1.graphics.beginFill(0xff0000);
				pnt1.graphics.drawCircle(38 + ptrn1.x, 67 + ptrn1.y, 2);
				pnt1.graphics.endFill();
				
				addChild(pnt1);
			
			var timer:Timer = new Timer(200);
				timer.addEventListener(TimerEvent.TIMER, onUpdate);
				timer.start();
				
				
			
			var timerRandomPatterns:Timer = new Timer(1000);
				timerRandomPatterns.addEventListener(TimerEvent.TIMER, onUpdatePattern);
				timerRandomPatterns.start();	
				
		}
		
		protected function onUpdatePattern(event:TimerEvent):void
		{
			if(Math.abs(linePoints[1].x - linePoints[0].x) < 2
					&&
				Math.abs(linePoints[1].y - linePoints[0].y) < 2)
			{
				return;
			}
			
		
			var matrix:Matrix;
			
			var tempPt:Point = new Point;
			
			tempPt.x = (linePoints[1].x + linePoints[0].x) * 0.5;
			tempPt.y = (linePoints[1].y + linePoints[0].y) * 0.5;
			
			var alpha1:Number = Math.tan((tempPt.y - linePoints[1].y)/(tempPt.x - linePoints[1].x)) * (180/Math.PI);
			//trace("alpha1 = " + alpha1);
			
						var random:int;
			var bmp:Bitmap;
			
			trace("Pow = " + Math.atan2(privY - mouseY, privX - mouseX) * 180/Math.PI);
			
			var angle:Number = Math.abs(Math.atan2(privY - mouseY, privX - mouseX) * 180/Math.PI);
			
			if(angle > 90)
			{
				angle -= 180;
			}
			
			if(Math.abs(angle) > 45)
			{
				
				random = Math.random() * (PATTERNS_BIG.length - 1);
				
				bmp = new PATTERNS_BIG[random];
			
			}else{
				
				random = Math.random() * (PATTERNS_SMALL.length - 1);
				
				bmp = new PATTERNS_SMALL[random];
			}
			
			privX = mouseX;
			privY = mouseY;
			
			
			bmp.alpha = 0;
			addChild(bmp);
			
			
			
			
			Tweener.addTween(bmp, {alpha: 1, time: 2}); 
			
			bmp.smoothing = true;
			
			bmp.x = tempPt.x - bmp.width/2;
			bmp.y = tempPt.y - bmp.height/2;
			
			matrix = bmp.transform.matrix;
			//MatrixTransformer.rotateAroundInternalPoint(matrix, b, 50, 45);
			
			bmp.transform.matrix = matrix;
			
			bmp.smoothing = true;
		
		}
		
	
		protected function onUpdate(event:TimerEvent):void
		{
			var pt:LinePoint = new LinePoint;
				pt.x = mouseX;
				pt.y = mouseY;
			
			pt.width = 0.005 * (Math.abs(400 - mouseY));
			
			if(int(pt.width) < 0.5)
			{
				pt.width = 0.5;
			}
			
			linePoints.push(pt);
			
			if(linePoints.length < 3)
			{
				return;
			}
			
			var temp:Vector.<LinePoint> = calculateSmoothLinePoints();
			
			if(temp)
			{
				
				renderLines(temp);
			}
			
		}
		
		
	
		private function calculateSmoothLinePoints():Vector.<LinePoint>
		{
			if(linePoints.length > 2)
			{
				var smoothedPoints:Vector.<LinePoint> = new Vector.<LinePoint>;
				
				for(var i:int = 2; i < linePoints.length; i++)
				{
					var prev2:LinePoint = linePoints[i-2];
					var prev1:LinePoint = linePoints[i-1];
					var cur:LinePoint = linePoints[i];
					
					var midPoint1:Point = new Point((prev1.x + prev2.x) * 0.5, (prev1.y + prev2.y) * 0.5);
					var midPoint2:Point = new Point((cur.x + prev1.x)*0.5, (cur.y + prev1.y) * 0.5);
					
					var segmentDistance:Number = 2;
					var distance:Number = Math.sqrt(Math.pow((midPoint2.x - midPoint1.x),2) + Math.pow((midPoint2.y - midPoint1.y), 2));
					var numberOfSegments:Number = Math.min(128,Math.max(int(distance/segmentDistance), 32));
					
					var t:Number = 0.0;
					var step:Number = 1.0/numberOfSegments;
					
					
					for(var j:int = 0; j < numberOfSegments; j++)
					{
						var newPoint:LinePoint = new LinePoint;
						
						
						// ccpAdd(ccpAdd(ccpMult(midPoint1, powf(1 - t, 2)), ccpMult(prev1.pos, 2.0f * (1 - t) * t)), ccpMult(midPoint2, t * t));
						newPoint.x = midPoint1.x * Math.pow(1-t, 2) + prev1.x * (2*(1-t)*t) + midPoint2.x * (t*t);
						newPoint.y = midPoint1.y * Math.pow(1-t, 2) + prev1.y * (2*(1-t)*t) + midPoint2.y * (t*t);
						// powf(1 - t, 2) * ((prev1.width %2B prev2.width) * 0.5f) %2B 2.0f * (1 - t) * t * prev1.width %2B t * t * ((cur.width %2B prev1.width) * 0.5f);
						newPoint.width = Math.pow((1-t), 2) * (prev1.width + prev2.width) * 0.5 + 2 * (1-t) * t * prev1.width + t * t * ((cur.width + prev1.width) * 0.5); 
						
					
						
						
						smoothedPoints.push(newPoint);
						t += step;
						
					}
					
					var finalPoint:LinePoint = new LinePoint;
					finalPoint.x = midPoint2.x;
					finalPoint.y = midPoint2.y;
					finalPoint.width = (cur.width + prev1.width) * 0.5;
					
					smoothedPoints.push(finalPoint);
					
					
				}
				
				linePoints.splice(0, 1);
				
				
				return smoothedPoints;
				
			}else{
			
				return null;
			}
		}
		
		[Embed(source="texture1.jpg")]
		private var TEXTURE1:Class;
		
		
		private var prevValue:Number;
		private	var prevPoint:Point;
		
		private var ppp:int = 0;
		
		private function renderLines(pts:Vector.<LinePoint>):void
		{
			
			for(var i:int = 1; i < pts.length; i++)
			{
				
				prevValue = pts[i-1].width;
				prevPoint = new Point(pts[i-1].x, pts[i-1].y);
				
				
				var curValue:Number = pts[i].width;
				
				var curPoint:Point = new Point(pts[i].x, pts[i].y);
				
				
				var dir:Point = new Point(curPoint.x - prevPoint.x, curPoint.y - prevPoint.y);
				
				var perpendicular:Point;
				
				if(dir.x > dir.y)
				{
					perpendicular = new Point(-dir.x, dir.y);
				
				}else{
				
					perpendicular = new Point(-dir.x, dir.y);
				}
			
				
				var vectLength:Number = Math.sqrt(Math.pow(perpendicular.x, 2) + Math.pow(perpendicular.y, 2));
				
				// TODO может быть деление на 0, поставить проверку
				var normolize:Point = new Point(perpendicular.x/vectLength, perpendicular.y/vectLength);
				
				var A:Point;
				var B:Point;
				
				if(i == 1)
				{
					 A = new Point(prevPoint.x - normolize.y * prevValue/2,
								   prevPoint.y - normolize.x * prevValue/2);
				
				     B = new Point(prevPoint.x + normolize.y * prevValue/2,
								   prevPoint.y + normolize.x * prevValue/2);
				
				}else{
					
				
					A = curveVerticies[curveVerticies.length - 1].c;
					
					B = curveVerticies[curveVerticies.length - 1].d;
					
		
				}
				
				var C:Point = new Point(curPoint.x - normolize.y * curValue/2,
					curPoint.y - normolize.x * curValue/2);
				
				
				var D:Point = new Point(curPoint.x + normolize.y * curValue/2,
					curPoint.y + normolize.x * curValue/2);
				
				curveVerticies.push({"a": A, "b": B, "c": C, "d": D});
	
				
				var bitmap:Bitmap = new TEXTURE1;
				
				graphics.beginFill(0xffffff);
				//graphics.beginBitmapFill(bitmap.bitmapData);
				//graphics.lineStyle(1, 0xff0000);
				graphics.moveTo(A.x, A.y);
				graphics.lineTo(B.x, B.y);
				graphics.lineTo(C.x, C.y);
				graphics.lineTo(A.x, A.y);
				
				//graphics.lineStyle(1, 0x0000ff);
				graphics.moveTo(B.x, B.y);
				graphics.lineTo(D.x, D.y);
				graphics.lineTo(C.x, C.y);
				graphics.lineTo(B.x, B.y);
				graphics.endFill();
			}
			
			//Tweener.addCaller(this, {onUpdate: drawAnim, time: 0.05, count:pts.length, transition:"easeinquad"});
		
			
		
		}
		
		private function drawAnim():void{
			graphics.beginFill(0xffffff);
			//graphics.beginBitmapFill(bitmap.bitmapData);
			//graphics.lineStyle(1, 0xff0000);
			graphics.moveTo(curveVerticies[ppp].a.x, curveVerticies[ppp].a.y);
			graphics.lineTo(curveVerticies[ppp].b.x, curveVerticies[ppp].b.y);
			graphics.lineTo(curveVerticies[ppp].c.x, curveVerticies[ppp].c.y);
			graphics.lineTo(curveVerticies[ppp].a.x, curveVerticies[ppp].a.y);
			
			//graphics.lineStyle(1, 0x0000ff);
			graphics.moveTo(curveVerticies[ppp].b.x, curveVerticies[ppp].b.y);
			graphics.lineTo(curveVerticies[ppp].d.x, curveVerticies[ppp].d.y);
			graphics.lineTo(curveVerticies[ppp].c.x, curveVerticies[ppp].c.y);
			graphics.lineTo(curveVerticies[ppp].b.x, curveVerticies[ppp].b.y);
			graphics.endFill()
			
			ppp++;
		}
		
		private function startGame():void
		{
			bFail = false;
			
			drawnCoords =  new Vector.<Vector.<Number>>;
			
			if(overlayLayer)
			{
				overlayLayer.graphics.clear();
				removeChild(overlayLayer);
			}
			
			
			for(i = 0; i < dataReaders.length; i++)
			{
				
				if(tfDeltas[i])
				{
					removeChild(tfDeltas[i]);
				}
				
			}
			
			if(tfCurrentSegment)
			{
				removeChild(tfCurrentSegment);
			}
			
			graphics.clear();
			
			tfDeltas = new Vector.<TextField>;
			curvesFiles = new Vector.<File>;
			fileIndecies = new Vector.<int>;
			rawStrings = new Vector.<ByteArray>;
			dataReaders = new Vector.<DataReader>;
			
			spline = new Spline;
			
			
			overlayLayer = new Sprite;
			
			addChild(overlayLayer);
			
			var appDirectoryResource:File = File.desktopDirectory.resolvePath("curves_files");
			
			// список всех файлов
			var contents:Array = appDirectoryResource.getDirectoryListing();
			
			
			for(var i:int = 0; i < contents.length; i++)
			{
				if(!contents[i].isDirectory)
				{
					
					var fileName:String = contents[i].name;
						//убираем расширение .txt
						fileName = fileName.substr(0, fileName.length - 4);
					
					if(fileName.search(textInputFileBase.text) != -1)
					{
						var index:int = fileName.search("_");
						var curveNum:int = -1;
						
						if(index != -1)
						{
							curveNum = int(fileName.substr(index + 1, fileName.length - 1));
							
							curvesFiles.push(contents[i]);
							fileIndecies.push(curveNum);
						}
					}
				}
			}
			
			
			var rawByteArray:ByteArray = new ByteArray;
			
			fileStreamRead = new FileStream();
			
			for(i = 0; i < curvesFiles.length; i++)
			{
				fileStreamRead.open(curvesFiles[i], FileMode.READ);
				rawStrings.push(new ByteArray);
				fileStreamRead.readBytes(rawStrings[rawStrings.length - 1]);
				fileStreamRead.close();
			}
			
			
				
			var rawString:String = rawByteArray.toString();
			
			for(i = 0; i < rawStrings.length; i++)
			{
				dataReaders.push(new DataReader(this, overlayLayer, rawStrings[i].toString()));
			}
			
			
			for(i = 0; i < dataReaders.length; i++)
			{
				totalContralPoints += dataReaders[i].controlPoints.length;
			}
		
			tfCurrentSegment = new TextField;
			tfCurrentSegment.x = 1100;
			tfCurrentSegment.y = 30;
			tfCurrentSegment.text = "Current segment: ";
			tfCurrentSegment.autoSize = TextFieldAutoSize.LEFT;
			
			
			addChild(tfCurrentSegment);
			
		
			for(i = 0; i < dataReaders.length; i++)
			{
				tfDeltas[i] = new TextField;
				tfDeltas[i].x = 1100;
				tfDeltas[i].y = i * 40 + 100;
				tfDeltas[i].autoSize = TextFieldAutoSize.LEFT;
				tfDeltas[i].text = "Delta" + i + ":";
				addChild(tfDeltas[i]);
				
				averageDeltas[i] = 0;
				currentDeltas[i] = 0;
			}
			
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			
			drawnCoords = new Vector.<Vector.<Number>>;
		
		}
		
		protected function onMouseMove(event:MouseEvent):void
		{
			if(bFail)
			{
				return;
			}
			
			
		 	if(isMouseDown){
			
				const ELLIPSE_WIDTH:Number = 10;
				
				graphics.beginFill(0xffff00, 1);
				graphics.drawEllipse(mouseX - ELLIPSE_WIDTH/2, mouseY - ELLIPSE_WIDTH/2, ELLIPSE_WIDTH, ELLIPSE_WIDTH);
				graphics.endFill();
				
				overlayLayer.graphics.beginFill(0x0, 1);
				overlayLayer.graphics.drawEllipse(mouseX, mouseY, 5, 5);
				overlayLayer.graphics.endFill();	
				
				drawnCoords.push(new Vector.<Number>);
				drawnCoords[drawnCoords.length - 1][0] = mouseX;
				drawnCoords[drawnCoords.length - 1][1] = mouseY;
				
				if(drawnCoords.length > 1)
				{
					graphics.lineStyle(ELLIPSE_WIDTH/2, 0x132288, 1, true, "normal", CapsStyle.ROUND);
					graphics.moveTo(drawnCoords[drawnCoords.length - 2][0], drawnCoords[drawnCoords.length - 2][1]);
					
					if(!bMouseWasReleased)
					{
						graphics.lineTo(drawnCoords[drawnCoords.length - 1][0], drawnCoords[drawnCoords.length - 1][1]);
						bMouseWasReleased = false;
					}
				}
				
				var approxY:Number = 0;
				
				for(var i:int = 0; i < dataReaders.length; i++)
				{
					
					tempArYForSpline = new Vector.<Number>;
					
					if(!dataReaders[i].bReverse)
					{
						for(var j:int = 0; j < dataReaders[i].data.length; j++)
						{
							tempArYForSpline.push(dataReaders[i].data[j][1]);
						}
						
						approxY =  Spline.returnApproximateY(dataReaders[i].data[0][0], dataReaders[i].data[dataReaders[i].data.length - 1][0],
															dataReaders[i].data.length, tempArYForSpline, mouseX);
						
		
					}else{
					
						for(j = 0; j < dataReaders[i].data.length; j++)
						{
							tempArYForSpline.push(dataReaders[i].data[j][0]);
						}
						
						approxY =  Spline.returnApproximateY(dataReaders[i].data[0][1], dataReaders[i].data[dataReaders[i].data.length - 1][1],
							dataReaders[i].data.length, tempArYForSpline, mouseY);
					}
					
					numDelta ++;
					
					if(!dataReaders[i].bReverse)
					{
						averageDeltas[i] += approxY - mouseY;
						
						currentDeltas[i] = approxY - mouseY;
						tfDeltas[i].text = "Delta" + i + ": " + (approxY - mouseY);
					}else{
					
						averageDeltas[i] += approxY - mouseX;
						
						currentDeltas[i] = (approxY - mouseX);
						tfDeltas[i].text = "Delta" + i + ": " + (approxY - mouseX);
					}
				
					
				}
				
				// ищем наименьшую delta
				var min:Number = Math.abs(currentDeltas[0]);
				var segmentNum:Number = 0;
				
				for(i = 0; i < currentDeltas.length; i++)
				{
					if(Math.abs(currentDeltas[i]) < min)
					{
						min = Math.abs(currentDeltas[i]);
						segmentNum = i;
					}
					
				}
				
				const EPSILON:Number = 10;
				
				if(min > EPSILON)
				{
					trace("fail");
					bFail = true;
				}
				
				var mousePt:Point = new Point(mouseX, mouseY);
				
				for(i = 0; i < dataReaders[segmentNum].controlPoints.length; i++)
				{
					if(dataReaders[segmentNum].controlPoints[i].x < mouseX + EPSILON
						                                    &&
					   dataReaders[segmentNum].controlPoints[i].x > mouseX - EPSILON)
					{
						if(dataReaders[segmentNum].controlPoints[i].y < mouseY + EPSILON
							&&
							dataReaders[segmentNum].controlPoints[i].y > mouseY - EPSILON)
						{
						   if(!dataReaders[segmentNum].overPoints[i])
						   {
						   		dataReaders[segmentNum].overPoints[i] = true;
								
								totalContralPoints --;
								trace(totalContralPoints);
								
								if(totalContralPoints == 0)
								{
									trace("SUCCESS SUCCESS");
									
								}
						   }
						}
					}
				}
				
				
				tfCurrentSegment.text = "Current segment: "  + segmentNum;
		
			}
			
		}
		
		protected function onMouseUp(event:MouseEvent):void
		{
			isMouseDown = false;
			bMouseWasReleased = true;
			
		}
		
		protected function onMouseDown(event:MouseEvent):void
		{
			isMouseDown = true;
			
			
		
		}
	
		
	}
}