package
{
	public class LinePoint
	{
		public var x:Number = 0;
		public var y:Number = 0;
		public var width:Number = 0;
		
		
		public function LinePoint(_x:Number = 0, _y:Number = 0, _width:Number = 0)
		{
			x = _x;
			y = _y;
			width = _width;
		}
	}
}