package
{
	import flash.display.Sprite;

	public class DataWriter
	{
		private  const ELLIPSE_WIDTH:Number = 20;
		
		private var _canvas:Sprite;
		private var _overlayLayer:Sprite;
		
		private var _privMouseX:Number;
		private var _privMouseY:Number;
		
		private var notFirst:Boolean = false;
		
		public  var data:Vector.<Vector.<Number>> = new Vector.<Vector.<Number>>;
		
	
		public function DataWriter(canvas:Sprite, 
								   overlayLayer:Sprite):void
		{
			_canvas = canvas;
			_overlayLayer = overlayLayer;
		}

		public function update():void
		{
			writeData(_canvas.mouseX, _canvas.mouseY);
		}
		
		public function writeData(x:Number, y:Number):void
		{
			if(!notFirst)
			{
				_privMouseX = x;
				_privMouseY = y;
				
				drawCircle(x, y);
				data.push(new <Number>[x, y]);
				
				notFirst = true;
				
			}else{
				
				var distance:uint = uint(x - _privMouseX);
			
				
					drawCircle(_privMouseX + 10, y);
					data.push(new <Number>[_privMouseX + 1, y]);
					
					_privMouseX = _privMouseX + 10;
					_privMouseY = y;
					
				
			}
		}
		
		private function drawCircle(x:Number, y:Number):void
		{
			if(_canvas)
			{
				
				_canvas.graphics.beginFill(0x0, 1);
				_canvas.graphics.drawEllipse(x - ELLIPSE_WIDTH/2, y - ELLIPSE_WIDTH/2, ELLIPSE_WIDTH, ELLIPSE_WIDTH);
				_canvas.graphics.endFill();
				
				_overlayLayer.graphics.beginFill(0xff0000, 1);
				_overlayLayer.graphics.drawEllipse(x, y, 5, 5);
				_overlayLayer.graphics.endFill();
			}
		}
	}
}