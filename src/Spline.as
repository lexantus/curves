package
{
	import flash.utils.getTimer;

	public class Spline
	{
		private static var a:Number;//[a, b] отрезок интерполяции
		private static var b:Number;
		
		private static var n:Number;// количество узлов
		
		private static var h:Number; // шаг интерполяции
		
		private static var arY:Vector.<Number>;
		private static var arX:Vector.<Number>;
		
		private static var m:Vector.<Number>;
		
		private static var x:Number;
		
		private static var y:Number;
		
		
		public static function returnApproximateY(_a:Number, _b:Number, _n:Number, 
							   _arY:Vector.<Number>,
							   _x:Number//для какого 'x' хотим найти 'y'
							   ):Number
		{
			arX = new Vector.<Number>;
			arY = _arY;
			m = new Vector.<Number>;
			
			var startTime:Number = getTimer();
			
			// ввод a, b, n
		 	a = _a;
			b = _b;
			n = _n-1;
			
			
			h = (b - a) / n;
		
			for(var i:int = 0; i <= n; i++)
			{
				arX.push(a + i * h);		
			}
			
			// вычисление первой производной
			m[0] = (4 * arY[1] - arY[2] - 3 * arY[0])/ (2*h);
			
			for(i = 1; i <= n-1; i++)
			{
				// вычисление остальных производных
				m[i] = (arY[i+1] - arY[i-1]) / (2*h);
			}
			
			// вычисление n-ой производной
			m[n] = (3 * arY[n] + arY[n-2] - 4 * arY[n-1]) / (2*h);
			
			
			// ввод x
			x = _x;
			
			
			// вычисляем y
			i = int((x - a) / h);
			
			if(i >= n)
			{
				return arY[n];
			}
			
			if(i < 0)
			{
				return arY[0];
			}
			
			y =(((arX[i+1] - x)* (arX[i+1] - x)*(2*(x-arX[i])+h)))/(h*h*h)*arY[i];
			y = y + (((x-arX[i])*(x - arX[i])*(2 * (arX[i+1] - x) + h))/(h*h*h))*arY[i+1];
			y = y + ((((arX[i+1] - x)*(arX[i+1] - x))*(x-arX[i]))/(h*h)) * m[i];
			y = y + ((((x-arX[i])*(x-arX[i]))*(x - arX[i+1]))/(h*h))*m[i+1];
			
			
			// нашли y
			var duration:Number = getTimer() - startTime;
			
			//trace("Spline интерполяция на отрезке [" + a + "," + b +"] количество узлов = " + n + " была выполнена за " + duration);
			
			return y;
		}
	}
}